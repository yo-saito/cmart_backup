<%@ page contentType="text/html; charset=utf-8" import="java.sql.*,java.lang.*,java.sql.*" %>

<html style="height:100%">
<head>

<script>
window.onunload = function() {};
history.forward();
</script>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cmart!! | Top</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/plagins/iCheck/square/blue.css">
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="Test.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-yellow" style="height:auto">
<!-- Site wrapper -->


<%

String user_name=(String)session.getAttribute("user_name");
if(user_name==null || user_name.isEmpty()){
  user_name="ログイン";
}
%>

<div class="wrapper">

  <header class="main-header">
    <!-- ロゴ -->
    <a href="Main.jsp" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>ma</span>
      <!-- logo for regular state and mobile devices -->
      <!--<span class="logo-lg"><b>C</b>mart!!</span>-->
      <img src="cmart-logo.png" height="55" width="210">
    </a>
    
      <nav class="navbar navbar-static-top" role="navigation" style="height:50px">
        <form action = "Result" method = "GET"  onsubmit="return check();">
          <input name="kagu_name" type="text" id="SWord" class = "SearchWord"
          placeholder="物品名・カテゴリー" pattern="*.*"  maxlength='20' required>
          <button type="submit" class="Searchbtn Searchfont">
            検索
          </button>
          <!--　ログイン状態変更処理　-->
        <!--　ログイン済状態　-->
        <%if(user_name!="ログイン"){%>
        <div class="navbar-custom-menu ">
          <!--<a href="Login.jsp" class="login-btn" style="font-size:28px">-->
          <!--ログイン-->
          <%=user_name%>さん
          <input type="button" value="ログアウト" onclick="location.href='Login.jsp'" class="btn-pos">
        </div>
        
        <!--　未ログイン状態　-->
        <%}else{%>
          <div class="navbar-custom-menu">
            <a href="Login.jsp" class="login-btn" style="font-size:28px">
              <!--ログイン-->
              <%=user_name%>
            </a>
          </div>
        <%}%>
        
        <!--　ログイン状態変更処理：ここまで　-->
        </form>
        
        
      </nav>
    </header>

  <!-- =============================================== -->
  <!-- 左側サイドバー関連 -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- サイドバー内カテゴリー検索一覧 -->
      <!-- 下にコメントアウトの状態で<li>のテンプレートあり -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">
          <h2>
            カテゴリー
          </h2>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=table">
              <span>テーブル</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=desk">
            <span>デスク</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=chair">
            <span>椅子</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=ラック">
            <span>ラック</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=light">
            <span>ライト</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=Pen stand">
            <span>ペン立て</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=pencil case">
            <span>ペンケース</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=cushion">
            <span>クッション</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=Fan">
            <span>USB式卓上扇風機</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=others">
            <span>その他</span>
          </a>
        </li>
        <!--<li class="treeview">
          <a href="Search.jsp">
            <span>add</span>
          </a>
        </li>-->
      </ul>
    </section>
    <!-- サイドバー関連：ここまで -->
  </aside>

  <!-- =============================================== -->

  <!-- メインコンテンツ -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        メール
      </h1>
    </section>
    <br>
    <section class="content"id="sec2">
      <div class="row">
        <div class="col-md-9">
        <form action="MailToCmart" method="POST">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">運営へメール</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                <input name="subjects" class="form-control" placeholder="Subject:">
              </div>
              <div class="form-group">
                <textarea name="Mail_Text" id="MailText" class="form-control" style="height: 300px"placeholder="文面入力"></textarea>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <input type="hidden" name="user" value=<%=user_name%>>
                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> 送信</button>
              </div>
              <button type="button" class="btn btn-default" onclick="TextDelete()"><i class="fa fa-times"></i>文面削除</button>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        </form>
        <!-- 下線 -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-body no-padding">
              <div id="calendar">
              </div>
            </div>
          </div>
        </div>
        <!-- 下線：ここまで -->
      </div>
    </section>
  </div>
  
  

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
    <%
    if(user_name!="ログイン"){
    %>
      <a href="OrderRecordResult?user_name=<%=user_name%>">
      <%}else{%>
      <a href="Login.jsp" onclick="warning()">
      <%}%>
        <b>
          発注履歴はこちら
        </b>
      </a>
    </div>
    <strong>Copyright &copy; 2018 </strong> 
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script type = "text/javascript">

function warning(){
  if(window.alert('ログインしてください!!')){
		location.href = "Main.jsp";
	}
}

function TextDelete(){
  document.getElementById("MailText").value="";
}

function check(){
var SearchList = document.getElementById("SWord").value;
var list = "";
var count=0;
var ar = SearchList.split("");
for(var i=0;i<ar.length;i++){
  if(ar[i].match( /[^ぁ-んァ-ヶ-a-zA-Z0-9\u3400-\u9FFF]/ )){
    count++;
    if(ar[i]=="ー"){
      connt--;
    }
  }
}
if(count>0){
  alert("検索ワードに記号を含めないでください\n数字を使用の場合は半角入力にてお願い致します。");
  return false;
}
}
</script>


</body>
</html>
