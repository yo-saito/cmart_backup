<%@ page contentType="text/html; charset=utf-8" import="java.sql.*,java.lang.*" %>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Cmart!! | New Item's</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/plagins/iCheck/square/blue.css">
  <link rel="stylesheet" href="Test.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<%
String user_name=(String)session.getAttribute("user_name");
if(user_name==null || user_name.isEmpty()){
  user_name="ログイン";
}
%>
<!--メインコンテンツ-->
<div class="login-box">
  <!--ログインロゴ-->
  <div class="login-logo">
    <a href="Main.jsp">
      <img src="cmart-logo.png" height="55" width="210">
    </a>
  </div>
  <!--ログインロゴ：ここまで-->
  <div class="login-box-body">
    <p class="login-box-msg">
      <b>
        新規物品情報入力
      </b>
    </p>
    <!--新規物品登録フォーム-->
    <form method="POST" enctype="multipart/form-data" action="ItemRecordtest" onsubmit="return check()" name="form" >
      <!--物品名入力欄-->
      <div class="form-group has-feedback">
        <input type="text" class="form-control X" id="BWord" name="kagu_name" maxlength='20' placeholder="物品名">
        <span class="glyphicon glyphicon-gift form-control-feedback">
        </span>
      </div>
      <!--物品名入力欄：ここまで-->
      <!--物品写真アップロード欄-->
      <div class="form-group has-feedback FileUp">
        <input type="file" name="image_name" id="sFiles" class="FileUp FileUpText"/>
        <span class="glyphicon glyphicon-file form-control-feedback">
        </span>
      </div>
      <!--物品写真アップロード欄：ここまで-->
      <!--カテゴリー選択欄-->
      <div class="form-group has-feedback">
        <select name="category" class="cate-font">
          <option value="0" selected>カテゴリー選択</option>
          <option value="table">テーブル</option>
          <option value="desk">デスク</option>
          <option value="chair">椅子</option>
          <option value="rack">ラック</option>
          <option value="light">卓上ライト</option>
          <option value="Pen stand">ペン立て</option>
          <option value="pencil case">ペンケース</option>
          <option value="cushion">コットンクッション</option>
          <option value="Fan">USB式卓上扇風機</option>
          <option value="others">その他</option>
        </select>
      </div>
      <!--カテゴリー選択欄：ここまで-->
      <!--在庫数登録欄-->
      <div class="form-group has-feedback">
        <input type="number" class="form-control cate-font" name="zaiko" placeholder="在庫数" min="1">
        <span class="glyphicon glyphicon-plus-sign form-control-feedback">
        </span>
      </div>
      <!--在庫数登録欄：ここまで-->
      <!--物品説明入力欄-->
      <div class="form-group has-feedback">
        <textarea class="chars2" cols="45" rows="5" id="SWord" name="setumei" placeholder="改行込み100文字入力になります。"></textarea >
        <p id="disp2">
          改行込み100文字以内で入力してください。
        </p>
      </div>
      <input type="hidden" name ="useradss" value = <%=user_name%>> 
      <!--物品説明入力欄：ここまで-->
      <!--新規登録ボタンブロック-->
      <div class="row">
        <div class="col-xs-4 btn-float3">
          <button id="disp" type="submit" class="btn btn-warning btn-block btn-flat"  onClick = "return  check()">
            新　規　登　録
          </button>
        </div>
      </div>
      <!--新規登録ボタンブロック：ここまで-->
    </form>
    <!--新規物品登録フォーム：ここまで-->
  </div>
  <!-- /.login-box-body -->
</div>
<!-- メインコンテンツ：ここまで -->

<!--以下Script-->  
 
<!-- jQuery 3 -->
<script src="node_modules/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="node_modules/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="node_modules//AdminLTE-2.4.2/plagins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function() {
  var targets = document.getElementsByClassName('chars2');
  document.getElementById("disp2").style.display="none";
  for (var i=0 ; i<targets.length ; i++) {
    // ▼文字が入力されたタイミングでチェックする：
    targets[i].oninput = function () {
      var alertelement = this.parentNode.getElementsByClassName('alertarea');
      if( this.value.trim().length > 100 ) {
        // ▼空白を除いた入力文字数が100文字いじょうなら
        this.style.border = "3px solid red";
        document.getElementById("disp2").style.display="block";
        document.getElementById("disp").style.display="none";
      }else{
        this.style.border = "2px solid blue";
        document.getElementById("disp2").style.display="none";
        document.getElementById("disp").style.display="block";
      }
    }
  }
});
</script>
<script type = "text/javascript">
function check(){
var fileList = document.getElementById("sFiles").files;
var list = "";
var count=0;
for(var i=0; i<fileList.length; i++){
list += fileList[i].name + "<br>";
}
var ar = list.split("");
for(var i=0;i<ar.length;i++){
  if(ar[i]=="."){
    count++;
  }
}
if(count>=2){
  alert("「.」を2つ以上ファイル名使用するファイルは\nファイル名を変更し「.」を１個にしてください");
  return false;
}else{
  alert('物品登録登録が完了しました!!')
		location.href = "Main.jsp";
	}
}
</script>
<script type = "text/javascript">

function check(){
var SearchList = document.getElementById("BWord").value;
var SearchList2 = document.getElementById("SWord").value;
var list = "";
var count=0;
var ar = SearchList.split("");
for(var i=0;i<ar.length;i++){
  if(ar[i].match( /[０-９]/ )){
    count++;
  }
}var ar = SearchList2.split("");
for(var i=0;i<ar.length;i++){
  if(ar[i].match( /[０-９]/ )){
    count++;
  }
}
if(count>0){
  alert("物品名・説明文に全角数字を含めないでください\n数字をご利用の際はお手数ですが半角数字にて入力くださいますようお願い致します。");
  return false;
}
}
</script>
</body>
</html>
