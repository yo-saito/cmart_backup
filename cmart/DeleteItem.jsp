<%@ page language="java" contentType="text/html; charset=utf-8" import="java.sql.*,java.lang.*" %>

<html>
<head>

<script>
window.onunload = function() {};
history.forward();
</script>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cmart!! | Result</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/AdminLTE-2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/AdminLTE-2.4.2/plagins/iCheck/square/blue.css">
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="Test.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">

<%

String user_name=(String)session.getAttribute("user_name");
if(user_name==null || user_name.isEmpty()){
  user_name="ログイン";
}
%>

    <!-- ロゴ -->
    <a href="Main.jsp" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>ma</span>
      <!-- logo for regular state and mobile devices -->
      <!--<span class="logo-lg"><b>C</b>mart!!</span>-->
      <img src="cmart-logo.png" height="55" width="210">
    </a>
    
    <nav class="navbar navbar-static-top" role="navigation">
    <form action = "Result2" method = "GET">
        <input name="kagu_name" type="text" id="SWord" class="SearchWord"
          placeholder="物品名・カテゴリー[MyItems]で全件検索" pattern="*.*"  maxlength='20' required>
        <input name="user_name" type="hidden" value="<%=user_name%>">
        <button type="submit" class="Searchbtn">
          検索
        </button>
        <%if(user_name!="ログイン"){%>
        <div class="navbar-custom-menu ">
          <!--<a href="Login.jsp" class="login-btn" style="font-size:28px">-->
          <!--ログイン-->
          <%=user_name%>さん
          <input type="button" value="ログアウト" onclick="location.href='Login.jsp'" class="btn-pos">
        </div>
        
        <!--　未ログイン状態　-->
        <%}else{%>
          <div class="navbar-custom-menu">
            <a href="Login.jsp" class="login-btn" style="font-size:28px">
              <!--ログイン-->
              <%=user_name%>
            </a>
          </div>
        <%}%>
    </form>
        
        <!--　ログイン状態変更処理　-->
        <!--　ログイン済状態　-->
        
        
        <!--　ログイン状態変更処理：ここまで　-->
        
      </nav>
    </header>

  <!-- =============================================== -->
  <!-- 左側サイドバー関連 -->
  <aside class="main-sidebar">
    <section class="sidebar">
      <!-- サイドバー内カテゴリー検索一覧 -->
      <!-- 下にコメントアウトの状態で<li>のテンプレートあり -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><h2>カテゴリー</h2></li>
        <li class="treeview">
          <a href="Result2?kagu_name=table&user_name=<%=user_name%>">
              <span>テーブル</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result2?kagu_name=desk&user_name=<%=user_name%>">
            <span>デスク</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result2?kagu_name=chair&user_name=<%=user_name%>">
            <span>椅子</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result2?kagu_name=rack&user_name=<%=user_name%>">
            <span>ラック</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result2?kagu_name=light&user_name=<%=user_name%>">
            <span>ライト</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result2?kagu_name=pen stand&user_name=<%=user_name%>">
            <span>ペン立て</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result2?kagu_name=pencil cas&user_name=<%=user_name%>">
            <span>ペンケース</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result2?kagu_name=cushion&user_name=<%=user_name%>">
            <span>クッション</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result2?kagu_name=Fan&user_name=<%=user_name%>">
            <span>USB式卓上扇風機</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result2?kagu_name=others&user_name=<%=user_name%>">
            <span>その他</span>
          </a>
        </li>
        <!--<li class="treeview">
          <a href="Search.jsp">
            <span>add</span>
          </a>
        </li>-->
      </ul>
    </section>
    <!-- サイドバー関連：ここまで -->
  </aside>

  <!-- =============================================== -->

  <!-- メインコンテンツ -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        物品検索
      </h1>
    </section>
      
    <section class="content"id="sec2">
      <!--検索物品情報受け取り-->
      <%
      	request.setCharacterEncoding("UTF8");
      	String id = (String) request.getAttribute("id");
      	String name = (String) request.getAttribute("name");
      	String cate = (String) request.getAttribute("cate");
      	String zai = (String) request.getAttribute("zai");
      	String setu = (String) request.getAttribute("setu");
      	String usad = (String) request.getAttribute("usad");
      	String temp[] = new String[1000];
      	HttpSession s = request.getSession();
      	temp = (String[])s.getAttribute("result");
      	int count=0;
      	while(temp[count]!=null){
      	  count++;
      	}
      	int hit_num = count / 6;
      	if(name == null){
      	    //RequestDispatcher dispatch = request.getRequestDispatcher("Main.jsp");
            //dispatch.forward(request, response);
        }
      %>
      <!--検索物品情報受け取り：ここまで-->
      <div class="row">
        <div class="col-md-3">
          <h3>検索件数 : <%=hit_num%>件</h3>
          <!--検索物品情報掲載繰り返し文-->
          <%for(int i=0;i<hit_num;i++){%>
          <!-- /.info-box -->
            <div class="info-box bg-yellow">
              <div class="info-box-contentS">
                <img src=<%=temp[i*6+1] + ".jpg"%> class="float-img" align="top">
                  <table class="Item-infoM" border="2" rules="all">
                    <tr>
                      <td class="setuU">物品名</td>
                      <td class="setuS"><%=temp[i*6+1]%></td>
                    </tr>
                    <tr>
                      <td>物品カテゴリー</td>
                      <td name="cateGory"><%=temp[i*6+2]%></td>
                    </tr>
                    <tr>
                      <td>在庫数</td>
                      <td><%=temp[i*6+3]%></td>
                    </tr>
                    <tr>
                      <td>説明</td>
                      <td><%=temp[i*6+4]%></td>
                    <tr>
                  </table>
                  <!-- /.info-box-content -->
                  <input hidden type="text" value="<%=temp[i*6+5]%>">
              </div>
            </div>
              <div class="col-md-3-2">
                <!-- /. box -->
                <div class="box box-solid bg-yellow">
                  <div class="input-group">
                    <div class="input-group-btn btn-float4 ">
                      <form class="<%=i%>"action = "ItemRecordReID" method = "POST">
                        <input name="delete_id" type="hidden" value=<%=temp[i*6]%>>
                        <input name="kagu_name" type="hidden" value=<%=temp[i*6+1]%>>
                        <input name="user_name" type="hidden" value=<%=user_name%>>
                        <button type="submit" class="btn btn-primary btn-margin2" >削　　除</button>
                      </form>
                    </div>
                    <div class="input-group-btn btn-float4 ">
                      <form class="<%=i%>"action = "Check2" method = "GET">
                        <input name="delete_id" type="hidden" value=<%=temp[i*6]%>>
                        <input name="kagu_name" type="hidden" value=<%=temp[i*6+1]%>>
                        <input name="user_name" type="hidden" value=<%=user_name%>>
                        <button type="submit" class="btn btn-primary btn-margin2" >更　　新</button>
                      </form>
                    </div>
                    <!-- /btn-group -->
                  </div>
                </div>
              </div>
          <%}%>
          <!--検索物品情報掲載繰り返し文：ここまで-->
        </div>
        <%
            //HttpSession session = request.getSession();
            request.setAttribute("kagu_name",name);
            request.setAttribute("category",cate);
            request.setAttribute("zaiko",zai);
            request.setAttribute("setumei",setu);
            request.setAttribute("useradss",usad);
        %> 
        </div>
        <!--新規物品登録ページへのリンクブロック-->
        <section>
          <div class="row">
            <div class="col-md-3">
              <div class="box box-solid bg-red">
                <div class="box-body">
                  <p class="floatItem">
                    <%if(user_name!="ログイン"){%>
                    <a href="NewItem.jsp">
                  <%}else{%>
                    <a href="Login.jsp" onclick="warning()">
                  <%}%>
                      <b>
                        新規物品登録
                      </b>
                        はこちら
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--新規物品登録ページへのリンクブロック：ここまで-->
        <!-- 下線 -->
        <div class="col-md-9">
          <div class="box box-warning">
            <div class="box-body no-padding">
              <!-- THE CALENDAR -->
              <div id="calendar">
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- 下線：ここまで -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 </strong> 
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- SlimScroll -->
<script src="AdminLTE-2.4.2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script type = "text/javascript">

function warning(){
  if(window.alert('ログインしてください!!')){
		location.href = "Main.jsp";
	}
}

function check(){
var SearchList = document.getElementById("SWord").value;
var list = "";
var count=0;
var ar = SearchList.split("");
for(var i=0;i<ar.length;i++){
  if(ar[i].match( /[^ぁ-んァ-ヶ-a-zA-Z0-9\u3400-\u9FFF]/ )){
    count++;
    if(ar[i]=="ー"){
      connt--;
    }
  }
}
if(count>=1){
  alert("検索ワードに記号を含めないでください\n数字を使用の場合は半角入力にてお願い致します。");
  return false;
}
}
</script>
</body>
</html>
