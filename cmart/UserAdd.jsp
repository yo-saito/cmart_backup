<%@ page contentType="text/html; charset=utf-8" import="java.sql.*,java.lang.*" %>

<html>
<head>

<script>
window.onunload = function() {};
history.forward();
</script>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cmart!! | User Created</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/plagins/iCheck/square/blue.css">
  <link rel="stylesheet" href="Test.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<%
session.removeAttribute("user_name");
%>


  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition login-page">
<!--メインコンテンツ-->
<div class="login-box">
  <!--ログインロゴ-->
  <div class="login-logo">
    <a href="Main.jsp">
      <img src="cmart-logo.png" height="55" width="210">
    </a>
  </div>
  <!--ログインロゴ：ここまで-->
  <!--新規登録コンテンツ-->
  <div class="login-box-body">
    <p class="login-box-msg">
      <b>
        新規情報入力
      </b>
    </p>
    <!--新規登録入力フォーム-->
    <form action = "UserAddsMail" method = "POST" onsubmit="return check();">
      <!--ユーザー名入力欄-->
      <div class="form-group has-feedback">
        <input type="text" class="form-control" id="BWord" name="user_name" placeholder="ユーザー名" maxlength='20' required>
        <span class="glyphicon glyphicon-user form-control-feedback">
        </span>
        <p>
          半角英数字20文字以内
        </p>
      </div>
      <!--ユーザー名入力欄：ここまで-->
      <!--パスワード入力欄-->
      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="SWord" name="user_pass" placeholder="Password" minlength="8" maxlength='20' required>
        <span class="glyphicon glyphicon-lock form-control-feedback">
        </span>
        <p>
          半角英数字8文字以上20文字以内
          <br>
          アルファベット大小数字、各1文字ずつ
        </p>
      </div>
      <!--パスワード入力欄：ここまで-->
      <!--メールアドレス入力欄-->
      <div class="form-group has-feedback">
        <input type="mail" class="form-control" name="user_mail" placeholder="メールアドレス" maxlength='50' required>
        <span class="glyphicon glyphicon-user form-control-feedback">
        </span>
        <p>
          半角英数字50文字以内
        </p>
      </div>
      <!--メールアドレス入力欄：ここまで-->
      <div class="row">
        <!--登録ボタンブロック-->
        <div class="col-xs-4 btn-float3">
          <button type="submit" class="btn btn-primary btn-block btn-flat" onclick="disp3()">
            登　　　　録
          </button>
        </div>
        <!--登録ボタンブロック-->
        <!-- /.col -->
      </div>
    </form>
  </div>
</div>
<!--メインコンテンツ：ここまで-->

<!--以下Script-->

<!-- jQuery 3 -->
<script src="node_modules/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="node_modules/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="node_modules//AdminLTE-2.4.2/plagins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
<script type = "text/javascript">

function check(){
var SearchList = document.getElementById("BWord").value;
var SearchList2 = document.getElementById("SWord").value;
var list = "";
var count=0;
var Bcount=0;
var Scount=0;
var Ncount=0;
var Wword="";
var ar = SearchList.split("");
for(var i=0;i<ar.length;i++){
  if(ar[i].match( /[^a-zA-Z0-9-'-']/ )){
    count++;
  }
}var ar = SearchList2.split("");
for(var i=0;i<ar.length;i++){
if(ar[i].match( /[^a-zA-Z0-9-'-']/ )){
    count++;
  }
  if(ar[i].match( /[a-z]/ )){
    Scount++;
  }
  if(ar[i].match( /[A-Z]/ )){
    Bcount++;
  }
  if(ar[i].match( /[0-9]/ )){
    Ncount++;
  }
}
if(Scount==0){
Wword="小文字のアルファベットを使用してください"  
}
if(Bcount==0){
Wword=Wword+"\n大文字のアルファベットを使用してください"  
}
if(Ncount==0){
Wword=Wword+"\n数字を使用してください"  
}
if(count>0){
  alert("ユーザ名並びにパスワードは半角英数字にて入力ください。");
  return false;
}else if(Ncount==0||Bcount==0||Scount==0){
  alert(Wword);
  return false;
}
else{
  if(window.alert('ユーザー登録が完了しました!!')){
		location.href = "Login.jsp";
	}
}
}
</script>
</body>
</html>
