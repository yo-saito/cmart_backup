<%@ page contentType="text/html; charset=utf-8" import="java.sql.*,java.lang.*" %>

<html>
<head>

<script>
window.onunload = function() {};
history.forward();
</script>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cmart!! | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/plagins/iCheck/square/blue.css">
  <link rel="stylesheet" href="Test.css">
<%
session.removeAttribute("user_name");
%>


  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<!--メインコンテンツ--> 
<div class="login-box">
  <!--ログインロゴ-->
  <div class="login-logo">
    <a href="Main.jsp"><img src="cmart-logo.png" height="55" width="210"></a>
  </div>
  <!--ログインロゴ：ここまで-->
  <!--ログインコンテンツ-->
  <div class="login-box-body">
    <p class="login-box-msg">
      <b>
      ログイン情報入力
      </b>
    </p>
    <!--ログイン入力フォーム-->
    <form action = "LoginBean" method = "POST" onsubmit="return check();">
      <!--ユーザー名入力欄-->
      <div class="form-group has-feedback">
        <input type="text" class="form-control" id="BWord" name="user_name" placeholder="ユーザー名" maxlength='20' required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <!--ユーザー名入力欄：ここまで-->
      <!--パスワード入力欄-->
      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="SWord" name="user_pass" placeholder="Password" maxlength='20' minlength="8" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <!--パスワード入力欄：ここまで-->
      <div class="row">
        <!--ログインボタン・新規ユーザー登録ページへのリンク-->
        <div class="col-xs-4 btn-float3">
          <button type="submit" class="btn btn-primary btn-block btn-flat">
            ロ　グ　イ　ン
          </button>
          <br>
          <div class="LoginLink">
            <a href=UserAdd.jsp>
              新規登録はこちら
            <a>
          </div>
          <div class="LoginLink">
            <a href=UserDelete.jsp>
              ユーザー情報削除はこちら
            <a>
          </div>
        </div>
      <!--ログインボタン・新規ユーザー登録ページへのリンク：ここまで-->
      </div>
    </form>
  <!--ログイン入力フォーム：ここまで-->
  </div>
  <!--ログインコンテンツ：ここまで-->
</div>
<!--メインコンテンツ：ここまで-->

<!--　以下Script　-->

<!-- jQuery 3 -->
<script src="node_modules/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="node_modules/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="node_modules//AdminLTE-2.4.2/plagins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
<script type = "text/javascript">

function check(){
var SearchList = document.getElementById("BWord").value;
var SearchList2 = document.getElementById("SWord").value;
var list = "";
var count=0;
var ar = SearchList.split("");
for(var i=0;i<ar.length;i++){
  if(ar[i].match( /[^a-zA-Z0-9-'-']/ )){
    count++;
  }
}var ar = SearchList2.split("");
for(var i=0;i<ar.length;i++){
  if(ar[i].match( /[^a-zA-Z0-9-#]/ )){
    count++;
  }
}
if(count>0){
  alert("ユーザ名並びにパスワードは半角英数字にて入力ください。");
  document.getElementById("BWord").value="";
  document.getElementById("SWord").value="";
  return false;
}
}
}
</script>
</body>
</html>
