<%@ page language="java" contentType="text/html; charset=utf-8" import="java.sql.*,java.lang.*" %>

<html>
<head>

<script>
window.onunload = function() {};
history.forward();
</script>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cmart!! | Result</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/AdminLTE-2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/AdminLTE-2.4.2/plagins/iCheck/square/blue.css">
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="Test.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">

<%

String user_name=(String)session.getAttribute("user_name");
if(user_name==null || user_name.isEmpty()){
  user_name="ログイン";
}
%>

    <!-- ロゴ -->
    <a href="Main.jsp" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>ma</span>
      <!-- logo for regular state and mobile devices -->
      <!--<span class="logo-lg"><b>C</b>mart!!</span>-->
      <img src="cmart-logo.png" height="55" width="210">
    </a>
    
    <nav class="navbar navbar-static-top" role="navigation">
        <form action = "OrderRecordResult2" method = "GET"  onsubmit="return check();">
          <input name="Sword" type="text" id="SWord" class = "SearchWord"
          placeholder="物品名" pattern="*.*"  maxlength='20' required>
          <input name="user_name" type="hidden" value="<%=user_name%>">
          <button type="submit" class="Searchbtn">
            検索
          </button>
          <!--　ログイン状態変更処理　-->
        <!--　ログイン済状態　-->
        <%if(user_name!="ログイン"){%>
        <div class="navbar-custom-menu ">
          <!--<a href="Login.jsp" class="login-btn" style="font-size:28px">-->
          <!--ログイン-->
          <%=user_name%>さん
          <input type="button" value="ログアウト" onclick="location.href='Login.jsp'" class="btn-pos">
        </div>
        
        <!--　未ログイン状態　-->
        <%}else{%>
          <div class="navbar-custom-menu">
            <a href="Login.jsp" class="login-btn" style="font-size:28px">
              <!--ログイン-->
              <%=user_name%>
            </a>
          </div>
        <%}%>
        
        <!--　ログイン状態変更処理：ここまで　-->
        </form>
      </nav>
    </header>

  <!-- =============================================== -->
  <!-- 左側サイドバー関連 -->
  <aside class="main-sidebar">
    <section class="sidebar">
      <!-- サイドバー内カテゴリー検索一覧 -->
      <!-- 下にコメントアウトの状態で<li>のテンプレートあり -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><h2>カテゴリー</h2></li>
        <li class="treeview">
          <a href="Result?kagu_name=table">
              <span>テーブル</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=desk">
            <span>デスク</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=chair">
            <span>椅子</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=ラック">
            <span>ラック</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=light">
            <span>ライト</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=Pen stand">
            <span>ペン立て</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=pencil case">
            <span>ペンケース</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=cushion">
            <span>クッション</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=Fan">
            <span>USB式卓上扇風機</span>
          </a>
        </li>
        <li class="treeview">
          <a href="Result?kagu_name=others">
            <span>その他</span>
          </a>
        </li>
        <!--<li class="treeview">
          <a href="Search.jsp">
            <span>add</span>
          </a>
        </li>-->
      </ul>
    </section>
    <!-- サイドバー関連：ここまで -->
  </aside>

  <!-- =============================================== -->

  <!-- メインコンテンツ -->
  <div class="content-wrapper">
  <section class="content-header">
      <h1>
        物品検索
      </h1>
    </section>
    <section class="content"id="sec2">
      <!--検索物品情報受け取り-->
      <%
        request.setCharacterEncoding("UTF8");
        String user = (String) request.getAttribute("user");
        String kagu = (String) request.getAttribute("kagu");
        String days = (String) request.getAttribute("days");
        String temp[] = new String[1000];
        HttpSession s = request.getSession();
        temp = (String[])s.getAttribute("orderresult");
        int count=0;
        while(temp[count]!=null){
          count++;
        }
        int hit_num = count / 3;
      %>
      <!--検索物品情報受け取り：ここまで-->
      <div class="row">
        <div class="col-md-3">
          <h3>検索件数 : <%=hit_num%>件</h3>
          <!--検索物品情報掲載繰り返し文-->
          <!-- /.info-box -->
            <div class="info-box bg-yellow">
                <%for(int i=0;i<hit_num;i++){%>
              <div class="info-box-contentS">
                <table class="Item-infoM" border="2" rules="all">
                  <tr>
                    <td class="setuU">発注物品名 : </td>
                    <td class="setuS2"><%=temp[i*3+1]%></td>
                  </tr>
                  <tr>
                    <td>発注　日付 : </td>
                    <td><%=temp[i*3+2]%></td>
                  </tr>
                </table>
                <form action="OrderCancel" method="POST" onsubmit="return confirm('登録ユーザーにキャンセルすることを伝えましたか？')">
                <input name="user" type="hidden" value=<%=temp[i*3]%>>
                <input name="kagu" type="hidden" value=<%=temp[i*3+1]%>>
                <input name="day" type="hidden" value="<%=temp[i*3+2]%>">
                <input type="submit" class="btn btn-primary btn-margin2" value="キャンセル" onclick="return kakunin()">
                </form>
                <form action="MailTest2" method="POST" onsubmit="return confirm('以下の商品を発注しますか？\n<%=temp[i*3+1]%>')">
                <input name="user" type="hidden" value=<%=temp[i*3]%>>
                <input name="kagu" type="hidden" value=<%=temp[i*3+1]%>>
                <input name="day" type="hidden" value=<%=temp[i*3+2]%>>
                <input type="submit" class="btn btn-primary btn-margin2" value="追加　発注">
                </form>
              </div>
              <%}%>
            </div>
          <!--検索物品情報掲載繰り返し文：ここまで-->
        </div>
        </div>
        <br>
        <!--新規物品登録ページへのリンクブロック-->
        <section>
          <div class="row">
            <div class="col-md-3">
              <div class="box box-solid bg-red">
                <div class="box-body">
                  <p class="floatItem">
                    <%if(user_name!="ログイン"){%>
                    <a href="NewItem.jsp">
                  <%}else{%>
                    <a href="Login.jsp" onclick="warning()">
                  <%}%>
                      <b>
                        新規物品登録
                      </b>
                        はこちら
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--新規物品登録ページへのリンクブロック：ここまで-->
        <!-- 下線 -->
        <div class="col-md-9">
          <div class="box box-warning">
            <div class="box-body no-padding">
              <!-- THE CALENDAR -->
              <div id="calendar">
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- 下線：ここまで -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 </strong> 
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- SlimScroll -->
<script src="AdminLTE-2.4.2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script type = "text/javascript">

function warning(){
  if(window.alert('ログインしてください!!')){
		location.href = "Main.jsp";
	}
}

function additem(){
  document.getElementById('form').action="MailTest2";
}

function check(){
var SearchList = document.getElementById("SWord").value;
var list = "";
var count=0;
var ar = SearchList.split("");
for(var i=0;i<ar.length;i++){
  if(ar[i].match( /[^ぁ-んァ-ヶ-a-zA-Z0-9\u3400-\u9FFF]/ )){
    count++;
    if(ar[i]=="ー"){
      connt--;
    }
  }
}
if(count>=1){
  alert("検索ワードに記号を含めないでください\n数字を使用の場合は半角入力にてお願い致します。");
  return false;
}
}
</script>
</body>
</html>
