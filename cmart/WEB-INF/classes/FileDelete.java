import java.sql.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@WebServlet("/FileDelete")
public class FileDelete extends HttpServlet {
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String kagu = (String)request.getParameter("kagu");
        PrintWriter out = response.getWriter();
        Path path = Paths.get("aaa.jpg");
        //ファイルパスを取得する
        String str = path.toAbsolutePath().toString();
        File file = new File("/usr/local/tomcat/webapps/cmart/" + kagu + ".jpg");
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            // String sql = "delete from kagu2 where kagu_name = '" + kagu + "'";
            // int rs = stmt.executeUpdate(sql);
            if (file.exists()){
                if (file.delete()){
                    out.println("ファイルを削除しました");
                }else{
                    out.println("ファイルの削除に失敗しました");
                }
                // out.println("ファイルを見つけました");
            }else{
                out.println("ファイルが見つかりません");
                out.println("pass : " + str);
            }
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}