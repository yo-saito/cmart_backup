import java.sql.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/UserAddsMail")
public class UserAddsMail extends HttpServlet {
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String name = (String)request.getParameter("user_name");
        String mail = (String)request.getParameter("user_mail");
        String pass = (String)request.getParameter("user_pass");
        PrintWriter out = response.getWriter();
        //out.println("");
	    //out.println("");
        
        Connection conn = null;
        int kagu_id = 1;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            String sql = "select user_name,AES_DECRYPT(user_pass,'ENCRYPT-KEY'),AES_DECRYPT(user_mail,'ENCRYPT=KEY') from user";
            ResultSet rs = stmt.executeQuery(sql);
            
            // sql = "insert into usertest (user_name,user_pass) values ('" + user_name + "','" + user_pass  + "')";
            sql = "insert into user values ('" + name + "',AES_ENCRYPT('" + pass  + "','ENCRYPT-KEY'),AES_ENCRYPT('" + mail + "','ENCRYPT=KEY'))";
            int num = stmt.executeUpdate(sql);
/*
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/searchman", "root", "");

            Statement stmt = conn.createStatement();
            String sql = "select * from kagu";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                int id = rs.getInt("id");
                kagu_id++;
                //String name = rs.getString("name");
                //out.println("<p>");
                //out.println("ID :" + id + ", NAME:" + name);
                //out.println("</p>");
            }
            sql = "insert into kagu values (" + kagu_id + ",' " + msg + "')";
            int num = stmt.executeUpdate(sql);
*/
    		out.println("ユーザー登録が完了しました。");
    		
            RequestDispatcher dispatch = request.getRequestDispatcher("Login.jsp");
            dispatch.forward(request, response);    		
    	    rs.close();
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}
