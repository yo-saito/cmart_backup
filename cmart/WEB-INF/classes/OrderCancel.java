import java.sql.*;
import java.io.*;
//import java.util.*;
//import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.util.Date;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.util.Calendar;
import java.text.SimpleDateFormat;

@WebServlet("/OrderCancel")
public class OrderCancel extends HttpServlet {
    
    private static final String ENCODE = "ISO-2022-JP";    
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String name = "";
        Connection conn = null;
        String user = (String)request.getParameter("user");
        String kagu = (String)request.getParameter("kagu");
        String day = (String)request.getParameter("day");
        String sql = "";
        String sql2 = "";
        
        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            sql = "delete from OrderRecords where user ='" + user + "' and kagu = '" + kagu + "' and day = '" + day + "'";
            int delete = stmt.executeUpdate(sql);
            sql = "select * from kagu2 where kagu_name = '" + kagu + "'";
            ResultSet zaikos = stmt.executeQuery(sql);
            String zaiko = "";
            while(zaikos.next()){
                zaiko = zaikos.getString("zaiko");   
            }
            int zaikonum = Integer.parseInt(zaiko);
            zaikonum++;
            sql = "update kagu2 set zaiko = '" + zaikonum + "' where kagu_name = '" + kagu + "'";
            int update = stmt.executeUpdate(sql);
            
            Statement stmt2 = conn.createStatement();
            sql2 = "select AES_DECRYPT(user_mail,'ENCRYPT=KEY') from user where user_name = '" + user + "'";
            ResultSet rs2 = stmt2.executeQuery(sql2);
            String User="";
            
            while(rs2.next()){
                User = rs2.getString("AES_DECRYPT(user_mail,'ENCRYPT=KEY')");
            }
            
            new OrderCancel().send(User,kagu,day);
            
    		RequestDispatcher dispatch = request.getRequestDispatcher("Main.jsp");
            dispatch.forward(request, response);    	

            zaikos.close();
            stmt.close();

        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
    
    public void send(String name , String kagu_name,String day) throws IOException {
        Properties props = new Properties();

        // SMTPサーバーの設定。ここではgooglemailのsmtpサーバーを設定。
        props.setProperty("mail.smtp.host", "smtp.gmail.com");

        // SSL用にポート番号を変更。
        props.setProperty("mail.smtp.port", "465");

        // タイムアウト設定
        props.setProperty("mail.smtp.connectiontimeout", "60000");
        props.setProperty("mail.smtp.timeout", "60000");

        // 認証
        props.setProperty("mail.smtp.auth", "true");

        // SSLを使用するとこはこの設定が必要。
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.socketFactory.port", "465");

       //propsに設定した情報を使用して、sessionの作成
        final Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("cmart.test001@gmail.com", "cmart123456789");
            }
        });

        // ここからメッセージ内容の設定。上記で作成したsessionを引数に渡す。
        MimeMessage message = new MimeMessage(session);

        try {
            //送信元
            final Address addrFrom = new InternetAddress("cmart.test001@gmail.com", "Cmart!!", ENCODE);
            message.setFrom(addrFrom);

            //送信先
            final Address addrTo = new InternetAddress(name,"Cmart", ENCODE);
            final Address addrCc = new InternetAddress("cmart.test001@gmail.com","Cmart!!", ENCODE);
            message.addRecipient(Message.RecipientType.TO, addrTo);
            message.addRecipient(Message.RecipientType.BCC, addrCc);
            
            // メールのSubject
            message.setSubject("【Cmart!!】発注のキャンセルを確認しました。(自動送信メール)", ENCODE);

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setText("この度はCmart!!をご利用いただきありがとうございます。\nお客様より発注発注キャンセル下記の内容で受け付けました。"
            + "\nご確認いただき、内容に誤りなどございましたらお知らせください。\n\n"
            + "■キャンセル内容■\n" + "物品名：" + kagu_name + "発注日：" + day + "\n\n"
            + "またのご利用を心よりお待ち申し上げます。"
            + "\n\n\n----------------------------------------------------------------------------------------------------------------------------------------------------------------\n" 
            + "■Cmart!! \n"
            + "■会社名：CLINKS株式会社システム開発事業部 \n 〒104-0032　東京都中央区八丁堀一丁目10番7号　TMG八丁堀ビル10F\n"
            + "■担当者：堺井\n"
            + "■サイトURL：https://6f840674678c481b8eeeda0f6a2fcdc2.vfs.cloud9.ap-southeast-1.amazonaws.com/cmart/Main.jsp \n"
            + "■お問い合わせ \n Tell：03-6262-8135 \n Mail：cmart.test001@gmail.com \n"
            + "----------------------------------------------------------------------------------------------------------------------------------------------------------------", ENCODE);
            
            
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);
 
            message.setContent(mp);

            //Transport.send(message);
            
            // その他の付加情報。
            //message.addHeader("X-Mailer", "blancoMail 0.1");
            //message.setSentDate(new Date());
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // メール送信。
        try {
            Transport.send(message);
        } catch (AuthenticationFailedException e) {
            // 認証失敗
                 e.printStackTrace();
        } catch (MessagingException e) {
            // smtpサーバへの接続失敗
           e.printStackTrace();
           
        }
        
    }
}

