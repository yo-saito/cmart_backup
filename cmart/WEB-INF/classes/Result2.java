import java.sql.*;
import java.io.*;
import java.net.*;
import java.lang.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import java.util.*;


@WebServlet("/Result2")
public class Result2 extends HttpServlet {
    
    //List<ResultBean> resultBean = new ArrayList<ResultBean>(); 
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String kagu_name = (String)request.getParameter("kagu_name");
        String user_name = (String)request.getParameter("user_name");
        PrintWriter out = response.getWriter();
        //out.println("");
        //out.println("検索結果");
	    //out.println("");
        
        List<ResultBean> kagu_list = new ArrayList<ResultBean>();
        
        Connection conn = null;
        int id_counter = 0;
        String[] result = new String[1000];
        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            String sql = "select * from kagu2 where kagu_name=\"" + kagu_name + "\" AND useradss =\"" + user_name + "\" or category =\"" + kagu_name + "\" AND useradss =\"" + user_name + "\"";
            ResultSet rs = stmt.executeQuery(sql);
            
            int num = 1;
            while(rs.next()){
                String id = rs.getString("kagu_id");
                String name = rs.getString("kagu_name");
                String cate = rs.getString("category");
                String zai = rs.getString("zaiko");
                String setu = rs.getString("setumei");
                String usad = rs.getString("useradss");
                
                kagu_list.add(new ResultBean(name,zai,cate));
                
                //out.println("<p>");
                //out.println("No :" + id_counter + ", Name : " + name + ", ID : "+ id + ", Text :" + setu);
                //out.println("</p>");
                num++;
                id_counter+=6;
            }
            
            request.setAttribute("kagu_list",kagu_list);
            request.setAttribute("hit_num",num);
            RequestDispatcher dispatch = request.getRequestDispatcher("Search.jsp");
            dispatch.forward(request, response);
            
    	    rs.close();
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }


    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String kagu_name = (String)request.getParameter("kagu_name");
        String user_name = (String)request.getParameter("user_name");
        PrintWriter out = response.getWriter();
        //out.println("");
        //out.println("検索結果");
	    //out.println("");
        

        Connection conn = null;
        int id_counter = 0;
        String[] result = new String[1000];
        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            Statement stmt2 = conn.createStatement();
            String sql;
            String sql2;
            // if(kagu_name.equals("all")){
            //     sql = "select * from kagu2 order by kagu_id asc";
            // }else 
            sql2 = "select AES_DECRYPT(user_mail,'ENCRYPT=KEY') from user where user_name = '" + user_name + "'";
            ResultSet rs2 = stmt2.executeQuery(sql2);
            while(rs2.next()){
                user_name = rs2.getString("AES_DECRYPT(user_mail,'ENCRYPT=KEY')");
            }
            if(kagu_name.equals("MyItems")){
                sql = "select * from kagu2 where useradss like \"%" + user_name + "%\"";
            }else{
                sql = "select * from kagu2 where (kagu_name like \"%" + kagu_name + "%\" or category like \"%" + kagu_name + "%\" ) AND useradss like \"%" + user_name + "%\"";
            }
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                String id = rs.getString("kagu_id");
                String name = rs.getString("kagu_name");
                String cate = rs.getString("category");
                String zai = rs.getString("zaiko");
                String setu = rs.getString("setumei");
                String usad = rs.getString("useradss");
                
                result[id_counter] = id;
                result[id_counter + 1] = name;
                result[id_counter + 2] = cate;
                result[id_counter + 3] = zai;
                result[id_counter + 4] = setu;
                result[id_counter + 5] = usad;
                
                
                //resultBean.getName();
                //resultBean.setZai(Integer.parseInt(zai));
                //resultBean.setSetu(setu);
                //resultBean.add(resultBean);
                //out.println("<p>");
                //out.println("No :" + id_counter + ", Name : " + name + ", ID : "+ id + ", Text :" + setu);
                //out.println("</p>");
                
                id_counter+=6;
            }
            
            HttpSession s1 = request.getSession();    
            s1.setAttribute("result",result);
            
            request.setAttribute("id",result[0]);
            request.setAttribute("name",result[1]);
            request.setAttribute("cate",result[2]);
            request.setAttribute("zai",result[3]);
            request.setAttribute("setu",result[4]);
            request.setAttribute("usad",result[5]);
            
            RequestDispatcher dispatch = request.getRequestDispatcher("DeleteItem.jsp");
            dispatch.forward(request, response);

    	    rs.close();
    	    rs2.close();
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}

