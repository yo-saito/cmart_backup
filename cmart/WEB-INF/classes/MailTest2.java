import java.sql.*;
import java.io.*;
//import java.util.*;
//import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.util.Date;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.util.Calendar;
import java.text.SimpleDateFormat;

@WebServlet("/MailTest2")
public class MailTest2 extends HttpServlet {
    
    private static final String ENCODE = "ISO-2022-JP";    
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String kagu_name = (String)request.getParameter("kagu");
        PrintWriter out = response.getWriter();
        String id = "";
        String name = "";
        String cate = "";
        String zai = "";
        String setu = "";
        String useradd = "";
        Connection conn = null;
        int id_counter = 1;
        String user = (String)request.getParameter("user");
        String kagu = (String)request.getParameter("kagu");
        String sql = "";
        String sql2 = "";
        
        Calendar cl = Calendar.getInstance();
        //SimpleDateFormatクラスでフォーマットパターンを設定する
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String days = (String)sdf.format(cl.getTime());
        
        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            sql = "select * from kagu2 where kagu_name=\"" + kagu_name + "\"";
            ResultSet rs = stmt.executeQuery(sql);
            
            Statement stmt2 = conn.createStatement();
            sql2 = "select AES_DECRYPT(user_mail,'ENCRYPT=KEY') from user where user_name = '" + user + "'";
            ResultSet rs2 = stmt2.executeQuery(sql2);
            String User="";
            
            while(rs2.next()){
                User = rs2.getString("AES_DECRYPT(user_mail,'ENCRYPT=KEY')");
            }

            while(rs.next()){
                id = rs.getString("kagu_id");
                name = rs.getString("kagu_name");
                cate = rs.getString("category");
                zai = rs.getString("zaiko");
                setu = rs.getString("setumei");
                useradd = rs.getString("useradss");
                
                //out.println("<p>");
                //out.println("発注前");
                //out.println("ID : " + id + ", 物品名 : " + name + ", 在庫 : "+ zai + ", 説明文 :" + setu);;
                //out.println("</p>");
                
                id_counter++;
            }

            int zai_after = Integer.parseInt(zai);
            zai_after--;
            int id_tmp = Integer.parseInt(id);

            if(zai_after >= 0){
                sql2 = "update kagu2 set zaiko =" + zai_after + " where kagu_id =" + id_tmp;
                int rs3 = stmt.executeUpdate(sql2);
                rs = stmt.executeQuery(sql);
                while(rs.next()){
                    id = rs.getString("kagu_id");
                    name = rs.getString("kagu_name");
                    cate = rs.getString("category");
                    zai = rs.getString("zaiko");
                    setu = rs.getString("setumei");
                    useradd = rs.getString("useradss");
                }
            }else{
                //out.println("在庫がありません!!");
            }
            
            
            String user_name = (String)request.getParameter("user");
            new MailTest().send(User,name,setu);
            new MailTest().send2(user_name,name,User,useradd);
            sql = "insert into orderrecord values ('" + user + "','" + kagu + "','" + days + "')";
            int num = stmt.executeUpdate(sql);
    		RequestDispatcher dispatch = request.getRequestDispatcher("Main.jsp");
            dispatch.forward(request, response);    	

    	    rs.close();
            stmt.close();
            rs2.close();
            stmt2.close();

        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
    
    public void send(String name , String kagu_name , String setu) throws IOException {
        Properties props = new Properties();

        // SMTPサーバーの設定。ここではgooglemailのsmtpサーバーを設定。
        props.setProperty("mail.smtp.host", "smtp.gmail.com");

        // SSL用にポート番号を変更。
        props.setProperty("mail.smtp.port", "465");

        // タイムアウト設定
        props.setProperty("mail.smtp.connectiontimeout", "60000");
        props.setProperty("mail.smtp.timeout", "60000");

        // 認証
        props.setProperty("mail.smtp.auth", "true");

        // SSLを使用するとこはこの設定が必要。
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.socketFactory.port", "465");

       //propsに設定した情報を使用して、sessionの作成
        final Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("cmart.test001@gmail.com", "cmart123456789");
            }
        });

        // ここからメッセージ内容の設定。上記で作成したsessionを引数に渡す。
        MimeMessage message = new MimeMessage(session);

        try {
            //送信元
            final Address addrFrom = new InternetAddress("cmart.test001@gmail.com", "Cmart!!", ENCODE);
            message.setFrom(addrFrom);

            //送信先
            final Address addrTo = new InternetAddress(name,"Cmart", ENCODE);
            final Address addrCc = new InternetAddress("cmart.test001@gmail.com","Cmart!!", ENCODE);
            message.addRecipient(Message.RecipientType.TO, addrTo);
            message.addRecipient(Message.RecipientType.BCC, addrCc);
            
            // メールのSubject
            message.setSubject("【Cmart!!】発注が完了しました(自動送信メール)", ENCODE);

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setText("この度はCmart!!をご利用いただきありがとうございます。\nお客様の発注を以下の内容で承りました。\nご確認いただき、内容に誤りなどございましたらお知らせください。\n\n"
            + "■ご発注内容■\n" + "物品名：" + kagu_name + "\n物品イメージ：添付ファイル\n物品説明："  + setu + "\n\n後ほど物品登録者様より物品受け渡しに関しましてご連絡があると思います。\n"
            + "そちらにて物品送付先などに関しましてはご相談いただきますようお願い致します。\nまたのご利用お待ちしております!!"
            + "\n\n\n----------------------------------------------------------------------------------------------------------------------------------------------------------------\n" 
            + "■Cmart!! \n"
            + "■会社名：CLINKS株式会社システム開発事業部 \n 〒104-0032　東京都中央区八丁堀一丁目10番7号　TMG八丁堀ビル10F\n"
            + "■担当者：堺井\n"
            + "■サイトURL：https://6f840674678c481b8eeeda0f6a2fcdc2.vfs.cloud9.ap-southeast-1.amazonaws.com/cmart/Main.jsp \n"
            + "■お問い合わせ \n Tell：03-6262-8135 \n Mail：cmart.test001@gmail.com \n"
            + "----------------------------------------------------------------------------------------------------------------------------------------------------------------", ENCODE);
            
            MimeBodyPart mbp2 = new MimeBodyPart();
            mbp2.attachFile("/usr/local/tomcat/webapps/cmart/" + kagu_name + ".jpg");
            
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);
            mp.addBodyPart(mbp2);
 
            message.setContent(mp);

            //Transport.send(message);
            
            // その他の付加情報。
            //message.addHeader("X-Mailer", "blancoMail 0.1");
            //message.setSentDate(new Date());
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // メール送信。
        try {
            Transport.send(message);
        } catch (AuthenticationFailedException e) {
            // 認証失敗
                 e.printStackTrace();
        } catch (MessagingException e) {
            // smtpサーバへの接続失敗
           e.printStackTrace();
           
        }
        
    }
    public void send2(String name , String kagu_name , String useradss, String kagu_add) throws IOException {
        Properties props = new Properties();

        // SMTPサーバーの設定。ここではgooglemailのsmtpサーバーを設定。
        props.setProperty("mail.smtp.host", "smtp.gmail.com");

        // SSL用にポート番号を変更。
        props.setProperty("mail.smtp.port", "465");

        // タイムアウト設定
        props.setProperty("mail.smtp.connectiontimeout", "60000");
        props.setProperty("mail.smtp.timeout", "60000");

        // 認証
        props.setProperty("mail.smtp.auth", "true");

        // SSLを使用するとこはこの設定が必要。
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.socketFactory.port", "465");

       //propsに設定した情報を使用して、sessionの作成
        final Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("cmart.test001@gmail.com", "cmart123456789");
            }
        });

        // ここからメッセージ内容の設定。上記で作成したsessionを引数に渡す。
        MimeMessage message = new MimeMessage(session);

        try {
            //送信元
            final Address addrFrom = new InternetAddress("cmart.test001@gmail.com", "Cmart!!", ENCODE);
            message.setFrom(addrFrom);

            //送信先
            final Address addrTo = new InternetAddress(kagu_add,"Cmart", ENCODE);
            message.addRecipient(Message.RecipientType.TO, addrTo);
            
            // メールのSubject
            message.setSubject("【Cmart!!】物品が発注されました(自動送信メール)", ENCODE);

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setText("この度はCmart!!をご利用いただきありがとうございます。\nお客様より登録いただいておりました、\n下記の商品がご注文されました。つきましてはご注文者様とご連絡して頂き、\n\n"
            + "■ご発注内容■\n" + "物品名：" + kagu_name + "\n" + "発注者名：" + name + "\n" + "発注者メールアドレス：" + useradss + "\n" 
            + "\n\n"
            + "物品発送につきましてご相談及びご確認して頂きますようお願い致します。\n\n今後ともご利用のほどお待ちしております。"
            + "\n\n\n----------------------------------------------------------------------------------------------------------------------------------------------------------------\n" 
            + "■Cmart!! \n"
            + "■会社名：CLINKS株式会社システム開発事業部 \n 〒104-0032　東京都中央区八丁堀一丁目10番7号　TMG八丁堀ビル10F\n"
            + "■担当者：堺井\n"
            + "■サイトURL：https://6f840674678c481b8eeeda0f6a2fcdc2.vfs.cloud9.ap-southeast-1.amazonaws.com/cmart/Main.jsp \n"
            + "■お問い合わせ \n Tell：03-6262-8135 \n Mail：cmart.test001@gmail.com \n"
            + "----------------------------------------------------------------------------------------------------------------------------------------------------------------------", ENCODE);
            
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);
 
            message.setContent(mp);

            //Transport.send(message);
            
            // その他の付加情報。
            //message.addHeader("X-Mailer", "blancoMail 0.1");
            //message.setSentDate(new Date());
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // メール送信。
        try {
            Transport.send(message);
        } catch (AuthenticationFailedException e) {
            // 認証失敗
                 e.printStackTrace();
        } catch (MessagingException e) {
            // smtpサーバへの接続失敗
           e.printStackTrace();
           
        }
    }
}

