import java.sql.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@WebServlet("/ItemRecordUpdate")
@MultipartConfig(location="/usr/local/tomcat/webapps/cmart/WEB-INF", maxFileSize=100000000)
public class ItemRecordUpdate extends HttpServlet {
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        // String id = (String)request.getParameter("kagu_id");
        String name = (String)request.getParameter("kagu_name");
        String before_name = (String)request.getParameter("before_name");
        String cate = (String)request.getParameter("category");
        String zai = (String)request.getParameter("zaiko");
        String setu = (String)request.getParameter("setumei");
        String usad = (String)request.getParameter("useradss");
        String Uplord_file = (String) request.getAttribute("image_name");
        
        PrintWriter out = response.getWriter();
        //out.println("");
	    //out.println("");
        
        Connection conn = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");
            Statement stmt = conn.createStatement();
            
            String sql = "select * from kagu2 order by kagu_id asc";
            ResultSet rs = stmt.executeQuery(sql);
            int flag = 0;
            while(rs.next()){
                String id_tmp = rs.getString("kagu_id");
                String name_tmp = rs.getString("kagu_name");
                String cate_tmp = rs.getString("category");
                String zai_tmp = rs.getString("zaiko");
                String setu_tmp = rs.getString("setumei");
                String usad_tmp = rs.getString("useradss");
                if(before_name.equals(name_tmp)){
                    flag = Integer.parseInt(id_tmp);
                }
            }

            
            sql = "update kagu2 set kagu_id = " + flag + " , kagu_name ='" + name + "' , category ='" 
            + cate + "' , zaiko =" + zai + " , setumei ='" + setu + "' where kagu_name = '" + before_name + "'";
            int num = stmt.executeUpdate(sql);
            
            Part part = request.getPart("image_name");
            if(Uplord_file!=null){
            String image_name = this.getFileName(part);
            part.write(getServletContext().getRealPath("/")  + image_name);
            //response.sendRedirect("/cmart/NewItem.jsp");
        
            //ファイル名の変更
            String file_path = getServletContext().getRealPath("/") + image_name;
            String new_file_path = getServletContext().getRealPath("/") + name + ".jpg";
            File fOld = new File(file_path);
            File fNew = new File(new_file_path);
        
            if(fOld.exists()){
                fOld.renameTo(fNew);
            }else{
                System.out.println("ファイル名の変更に失敗しました");
            }
            }else{
            
            String new_file_path = getServletContext().getRealPath("/") + name + ".jpg";
            File file = new File("/usr/local/tomcat/webapps/cmart/" + before_name + ".jpg");
            File fNew = new File(new_file_path);
            if(file.exists()){
                file.renameTo(fNew);
            }else{
                System.out.println("ファイル名の変更に失敗しました");
            }
            }
     		RequestDispatcher dispatch = request.getRequestDispatcher("Main.jsp");
            dispatch.forward(request, response);
    	    rs.close();
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
    
    private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }
}

