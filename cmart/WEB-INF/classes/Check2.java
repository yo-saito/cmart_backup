import java.sql.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/Check2")
public class Check2 extends HttpServlet {
    
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String kagu_name = (String)request.getParameter("kagu_name");
        PrintWriter out = response.getWriter();
        //out.println("");
        //out.println("検索結果");
	    //out.println("");
        
        Connection conn = null;
        int id_counter = 0;
        String[] result = new String[1000];
        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            String sql = "select * from kagu2 where kagu_name=\"" + kagu_name + "\" or category =\"" + kagu_name + "\"";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                String id = rs.getString("kagu_id");
                String name = rs.getString("kagu_name");
                String cate = rs.getString("category");
                String zai = rs.getString("zaiko");
                String setu = rs.getString("setumei");
                String usad = rs.getString("useradss");
                
                result[id_counter] = id;
                result[id_counter + 1] = name;
                result[id_counter + 2] = cate;
                result[id_counter + 3] = zai;
                result[id_counter + 4] = setu;
                result[id_counter + 5] = usad;
                //out.println("<p>");
                //out.println("No :" + id_counter + ", Name : " + name + ", ID : "+ id + ", Text :" + setu);
                //out.println("</p>");
                
                id_counter+=6;
            }
            
            request.setAttribute("name",result[1]);
            request.setAttribute("cate",result[2]);
            request.setAttribute("zai",result[3]);
            request.setAttribute("setu",result[4]);
            request.setAttribute("usad",result[5]);
            RequestDispatcher dispatch = request.getRequestDispatcher("ItemUpdate.jsp");
            dispatch.forward(request, response);

    	    rs.close();
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}
