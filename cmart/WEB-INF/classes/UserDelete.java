import java.sql.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/UserDelete")
public class UserDelete extends HttpServlet {
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String user_name = (String)request.getParameter("user_name");
        String user_pass = (String)request.getParameter("user_pass");
        PrintWriter out = response.getWriter();
        int flag=0;
        //out.println("");
	    //out.println("");
        
        Connection conn = null;
        int kagu_id = 1;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            String sql = "select user_name,AES_DECRYPT(user_pass,'ENCRYPT-KEY'),AES_DECRYPT(user_mail,'ENCRYPT=KEY') from user where user_name = '" + user_name + "' or user_mail = AES_ENCRYPT('" + user_name + "','ENCRYPT=KEY')";
            String sql2 = "select * from newuser";
            String sql3 = "select * from newuser";
            String Sadss = "";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                String name_tmp = rs.getString("user_name");
                String pass_tmp = rs.getString("AES_DECRYPT(user_pass,'ENCRYPT-KEY')");
                String mail_temp = rs.getString("AES_DECRYPT(user_mail,'ENCRYPT=KEY')");
                Sadss = mail_temp;
                if(user_name.equals(name_tmp)||user_name.equals(mail_temp)){
                    flag = 1;
                    if(user_pass.equals(pass_tmp)){
                        flag = 2;
                    }
                }
            }
            if(flag==2){
                sql = "delete from user where user_name = '"  + user_name + "' or user_mail = AES_ENCRYPT('" + Sadss + "','ENCRYPT=KEY')";
                sql2 = "delete from kagu2 where useradss = '"  + Sadss + "'";
                sql3 = "delete from orderrecord where user = '"  + user_name + "'";
                int num = stmt.executeUpdate(sql);
                int num2 = stmt.executeUpdate(sql2);
                int num3 = stmt.executeUpdate(sql3);
                out.println("<script type=\"text/javascript\">window.alert('ユーザー削除完了');location.href = \"/cmart/Main.jsp\";</script>");
                rs.close();
                stmt.close();
            }else if(flag == 1){
                out.println("<script type=\"text/javascript\">window.alert('パスワードが違います');location.href = \"/cmart/UserDelete.jsp\";</script>");
                rs.close();
                stmt.close();
            }else{
                out.println("<script type=\"text/javascript\">window.alert('一致するユーザが御座いません');location.href = \"/cmart/UserDelete.jsp\";</script>");
                rs.close();
                stmt.close();
            }
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}