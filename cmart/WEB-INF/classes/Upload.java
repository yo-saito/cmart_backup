import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/Upload")
@MultipartConfig(location="/usr/local/tomcat/webapps/cmart/WEB-INF", maxFileSize=536870912)
public class Upload extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part part = request.getPart("image_name");
        String name = this.getFileName(part);
        part.write(getServletContext().getRealPath("/")  + name);
        response.sendRedirect("/cmart/NewItem.jsp");
        
        //TODO ファイル名の変更
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String kagu_name = (String)request.getParameter("kagu_name");
        
        String file_path = getServletContext().getRealPath("/") + name;
        String new_file_path = getServletContext().getRealPath("/") + kagu_name + ".jpg";
        File fOld = new File(file_path);
        File fNew = new File(new_file_path);
        
        if(fOld.exists()){
            fOld.renameTo(fNew);
        }else{
            System.out.println("ファイル名の変更に失敗しました");
        }
    }

    private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }
}