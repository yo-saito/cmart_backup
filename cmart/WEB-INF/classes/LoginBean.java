import java.sql.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import javax.swing.JOptionPane;

@WebServlet("/LoginBean")
public class LoginBean extends HttpServlet {
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String user_name = (String)request.getParameter("user_name");
        String user_pass = (String)request.getParameter("user_pass");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(true);
        session.removeAttribute("user_name");
        
        Connection conn = null;

        try {
            
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            String sql = "select user_name,AES_DECRYPT(user_pass,'ENCRYPT-KEY'),AES_DECRYPT(user_mail,'ENCRYPT=KEY') from user where (user_name = '" 
            + user_name + "' or user_mail = AES_ENCRYPT('" 
            + user_name + "','ENCRYPT=KEY') ) and user_pass = AES_ENCRYPT('" 
            + user_pass + "','ENCRYPT-KEY')";
            ResultSet rs = stmt.executeQuery(sql);
            

            if(rs.next()){ 
                //out.println("");
                //out.println("ログインが完了しましたよ。");
                //out.println("");
                session.removeAttribute("user_name");
                user_name = rs.getString("user_name");
                session.setAttribute("user_name",user_name);
                rs.close();
                stmt.close();
                RequestDispatcher dispatch = request.getRequestDispatcher("Main.jsp");
                dispatch.forward(request, response);
                
            }else{ 
                //out.println("");
                //out.println("ログインが失敗しましたよ。");
                //out.println("");
                out.println("<script type=\"text/javascript\">window.alert('IDまたはパスワードが違います');location.href = \"/cmart/Login.jsp\";</script>");
                session.removeAttribute("user_name");
                rs.close();
                stmt.close();
                //JOptionPane.showMessageDialog(null,"ログインに失敗しました");
                //RequestDispatcher dispatch = request.getRequestDispatcher("Login.jsp");
                //dispatch.forward(request, response);
            }

        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}
