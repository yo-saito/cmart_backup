import java.sql.*;
import java.io.*;
import java.net.*;
import java.lang.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import java.util.*;


@WebServlet("/OrderRecordResult")
public class OrderRecordResult extends HttpServlet {
    
    //List<ResultBean> resultBean = new ArrayList<ResultBean>(); 

    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String user_name = (String)request.getParameter("user_name");
        PrintWriter out = response.getWriter();
        //out.println("");
        //out.println("検索結果");
	    //out.println("");
        

        Connection conn = null;
        int id_counter = 0;
        String[] orderresult = new String[1000];
        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            String sql;
            sql = "select * from OrderRecords where user = '" + user_name + "'";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                String user = rs.getString("user");
                String kagu = rs.getString("kagu");
                String days = rs.getString("day");
                
                orderresult[id_counter] = user;
                orderresult[id_counter + 1] = kagu;
                orderresult[id_counter + 2] = days;
                
                id_counter+=3;
            }
            
            HttpSession s1 = request.getSession();    
            s1.setAttribute("orderresult",orderresult);
            
            request.setAttribute("user",orderresult[0]);
            request.setAttribute("kagu",orderresult[1]);
            request.setAttribute("days",orderresult[2]);
            
            RequestDispatcher dispatch = request.getRequestDispatcher("OrderRecord.jsp");
            dispatch.forward(request, response);

    	    rs.close();
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}

