import java.sql.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/Review")
public class Review extends HttpServlet {
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String name = (String)request.getParameter("kagu");
        String point = (String)request.getParameter("point");
        String review = (String)request.getParameter("review");
        PrintWriter out = response.getWriter();
        
        Connection conn = null;
        int kagu_id = 1;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            
            String sql = "insert into review values ('" + name + "','" + review  + "','" + point + "')";
            int num = stmt.executeUpdate(sql);
            
    		out.println("<script type=\"text/javascript\">window.alert('レビューの登録が完了しました');location.href = \"/cmart/Test.jsp\";</script>");
    		
            RequestDispatcher dispatch = request.getRequestDispatcher("Main.jsp");
            dispatch.forward(request, response);
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}
