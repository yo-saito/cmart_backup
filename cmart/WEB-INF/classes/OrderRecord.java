import java.sql.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.Calendar;
import java.text.SimpleDateFormat;

@WebServlet("/OrderRecord")
public class OrderRecord extends HttpServlet {
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String user = (String)request.getParameter("user_name");
        String kagu = (String)request.getParameter("kagu_name");
        String sql = "";
        
        Calendar cl = Calendar.getInstance();
        //SimpleDateFormatクラスでフォーマットパターンを設定する
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String days = (String)sdf.format(cl.getTime());
        
        PrintWriter out = response.getWriter();
        
        Connection conn = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");
            Statement stmt = conn.createStatement();
            
            sql = "insert into OrderRecords values ('" + user + "','" + kagu + "','" + days + "')";
            int num = stmt.executeUpdate(sql);
    		
            RequestDispatcher dispatch = request.getRequestDispatcher("Test.jsp");
            dispatch.forward(request, response);
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}