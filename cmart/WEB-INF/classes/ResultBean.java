import java.sql.*;
import java.io.*;
import java.net.*;
import java.lang.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

public class ResultBean implements Serializable{
    
    private String name="";
    private String zai="";
    private String setu="";
    
    public ResultBean(String name,String zai,String setu){
        this.name = name;
        this.zai = zai;
        this.setu = setu;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setZai(String zai){
        this.zai = zai;
    }
    
    public String getZai(){
        return this.zai;
    }
    
    public void setSetu(String setu){
        this.setu = setu;
    }
    
    public String getSetu(){
        return this.setu;
    }
    
    public String toString(){
        return "";
    }
    
}