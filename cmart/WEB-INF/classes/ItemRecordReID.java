import java.sql.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@WebServlet("/ItemRecordReID")
@MultipartConfig(location="/usr/local/tomcat/webapps/cmart/WEB-INF", maxFileSize=100000000)
public class ItemRecordReID extends HttpServlet {
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        //String id = (String)request.getParameter("kagu_id");
        String user = (String)request.getParameter("user_name");
        String delete_id = (String)request.getParameter("delete_id");
        String name = (String)request.getParameter("kagu_name");
        PrintWriter out = response.getWriter();
        Connection conn = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");
            Statement stmt = conn.createStatement();
            Statement stmt2 = conn.createStatement();
            Path path = Paths.get(name + ".jpg");
            //ファイルパスを取得する
            String str = path.toAbsolutePath().toString();
            //ファイルのパスを指定する。
            File file = new File("/usr/local/tomcat/webapps/cmart/" + name + ".jpg");
            
            String sql2 = "select AES_DECRYPT(user_mail,'ENCRYPT=KEY') from user where user_name = '" + user + "'";
            ResultSet rs2 = stmt2.executeQuery(sql2);
            while(rs2.next()){
                user = rs2.getString("AES_DECRYPT(user_mail,'ENCRYPT=KEY')");
            }
            String sql = "delete from kagu2 where kagu_id = " + delete_id + " AND useradss like \"%" + user + "%\"";
            if (file.exists()){
                if (file.delete()){
                }else{
                    out.println("ファイルの削除に失敗しました");
                }
            }else{
            }
            stmt.execute(sql);
            
     		RequestDispatcher dispatch = request.getRequestDispatcher("Main.jsp");
            dispatch.forward(request, response);    	
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}

