import java.sql.*;
import java.io.*;
import java.net.*;
import java.lang.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

public class OrderBean implements Serializable{
    
    private String user_name="";
    private String kagu_name="";
    private String days="";
    
    public OrderBean(String user_name,String kagu_name,String days){
        this.user_name = user_name;
        this.kagu_name = kagu_name;
        this.days = days;
    }
    
    public void setUser(String name){
        this.user_name = name;
    }
    
    public String getUser(){
        return this.user_name;
    }
    
    public void setKagu(String kagu){
        this.kagu_name = kagu;
    }
    
    public String getZai(){
        return this.kagu_name;
    }
    
    public void setDays(String day){
        this.days = day;
    }
    
    public String getDays(){
        return this.days;
    }
    
    public String toString(){
        return "";
    }
    
}