import java.sql.*;
import java.io.*;
//import java.util.*;
//import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.util.Date;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.util.Calendar;
import java.text.SimpleDateFormat;

@WebServlet("/MailToCmart")
public class MailToCmart extends HttpServlet {
    
    private static final String ENCODE = "ISO-2022-JP";    
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String subject = (String)request.getParameter("subjects");
        String mail_text = (String)request.getParameter("Mail_Text");
        String user = (String)request.getParameter("user");
        PrintWriter out = response.getWriter();
        Connection conn = null;
        new MailToCmart().send(subject,mail_text,user);
		RequestDispatcher dispatch = request.getRequestDispatcher("Main.jsp");
        dispatch.forward(request, response);   
        out.close();
    }
    
    public void send(String subject , String mail_text,String user) throws IOException {
        Properties props = new Properties();

        // SMTPサーバーの設定。ここではgooglemailのsmtpサーバーを設定。
        props.setProperty("mail.smtp.host", "smtp.gmail.com");

        // SSL用にポート番号を変更。
        props.setProperty("mail.smtp.port", "465");

        // タイムアウト設定
        props.setProperty("mail.smtp.connectiontimeout", "60000");
        props.setProperty("mail.smtp.timeout", "60000");

        // 認証
        props.setProperty("mail.smtp.auth", "true");

        // SSLを使用するとこはこの設定が必要。
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.socketFactory.port", "465");

       //propsに設定した情報を使用して、sessionの作成
        final Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("cmart.test001@gmail.com", "cmart123456789");
            }
        });

        // ここからメッセージ内容の設定。上記で作成したsessionを引数に渡す。
        MimeMessage message = new MimeMessage(session);

        try {
            //送信元
            final Address addrFrom = new InternetAddress("cmart.test001@gmail.com", "Cmart!!", ENCODE);
            message.setFrom(addrFrom);

            //送信先
            final Address addrTo = new InternetAddress("cmart.test001@gmail.com", "Cmart!!", ENCODE);
            message.addRecipient(Message.RecipientType.TO, addrTo);
            
            // メールのSubject
            message.setSubject(subject, ENCODE);

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setText(mail_text + "\n\n■送信ユーザー : " + user, ENCODE);
            
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);
 
            message.setContent(mp);

            //Transport.send(message);
            
            // その他の付加情報。
            //message.addHeader("X-Mailer", "blancoMail 0.1");
            //message.setSentDate(new Date());
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // メール送信。
        try {
            Transport.send(message);
        } catch (AuthenticationFailedException e) {
            // 認証失敗
                 e.printStackTrace();
        } catch (MessagingException e) {
            // smtpサーバへの接続失敗
           e.printStackTrace();
        }
    }
}

