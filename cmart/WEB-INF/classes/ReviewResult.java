import java.sql.*;
import java.io.*;
import java.net.*;
import java.lang.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import java.util.*;


@WebServlet("/ReviewResult")
public class ReviewResult extends HttpServlet {
    
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String kagu = (String)request.getParameter("kagu");
        PrintWriter out = response.getWriter();
        
        Connection conn = null;
        String[] reviewresult = new String[1000];
        int id_counter = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");

            Statement stmt = conn.createStatement();
            String sql;
            sql = "select * from review where kagu_name = '" + kagu + "'";
            ResultSet rs = stmt.executeQuery(sql);
            
            String name="";
            String point="";
            String review="";

            while(rs.next()){
                name = rs.getString("kagu_name");
                point = rs.getString("point");
                review = rs.getString("review");
                
                reviewresult[id_counter] = name;
                reviewresult[id_counter+1] = point;
                reviewresult[id_counter+2] = review;
                id_counter+=3;
            }
            
            HttpSession s1 = request.getSession();    
            s1.setAttribute("reviewresult",reviewresult);
            
            request.setAttribute("kagu_name",reviewresult[0]);
            request.setAttribute("point",reviewresult[1]);
            request.setAttribute("review",reviewresult[2]);
            
            RequestDispatcher dispatch = request.getRequestDispatcher("Review.jsp");
            dispatch.forward(request, response);

    	    rs.close();
            stmt.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
}

