import java.sql.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/ItemRecordtest")
@MultipartConfig(location="/usr/local/tomcat/webapps/cmart/WEB-INF", maxFileSize=100000000)
public class ItemRecordtest extends HttpServlet {
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        //String id = (String)request.getParameter("kagu_id");
        String name = (String)request.getParameter("kagu_name");
        String cate = (String)request.getParameter("category");
        String zai = (String)request.getParameter("zaiko");
        String setu = (String)request.getParameter("setumei");
        String usad = (String)request.getParameter("useradss");
        
        PrintWriter out = response.getWriter();
        //out.println("");
	    //out.println("");
        
        Connection conn = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cmart", "root", "");
            Statement stmt = conn.createStatement();
            Statement stmt2 = conn.createStatement();
            
            String sql = "select * from kagu2 order by kagu_id asc";
            ResultSet rs = stmt.executeQuery(sql);
            String sql2 = "select AES_DECRYPT(user_mail,'ENCRYPT=KEY') from user where user_name = '" + usad + "'";
            ResultSet rs2 = stmt2.executeQuery(sql2);
            
            while(rs2.next()){
                usad = rs2.getString("AES_DECRYPT(user_mail,'ENCRYPT=KEY')");
            }
            int id_counter = 1;
            int flag = 0;
            while(rs.next()){
                String id_tmp = rs.getString("kagu_id");
                String name_tmp = rs.getString("kagu_name");
                String cate_tmp = rs.getString("category");
                String zai_tmp = rs.getString("zaiko");
                String setu_tmp = rs.getString("setumei");
                String usad_tmp = rs.getString("useradss");
                if(name.equals(name_tmp)){
                    flag = 1;
                }
                int id_check=Integer.parseInt(id_tmp);
                id_counter = id_check;
                id_counter++;
            }
            
            if(flag==1){
                out.println("<script type=\"text/javascript\">window.alert('同一物品名が既に存在します!!');location.href = \"/cmart/NewItem.jsp\";</script>");
                //RequestDispatcher dispatch = request.getRequestDispatcher("NewItem.jsp");
                //dispatch.forward(request, response);  
                rs2.close();
    	        rs.close();
                stmt.close();
                stmt2.close();
            }
            
            sql = "insert into kagu2 values ('" + id_counter + "','" + name + "','" + cate + "','" + zai + "','" + setu + "','" + usad + "')";
            int num = stmt.executeUpdate(sql);
    		
    		Part part = request.getPart("image_name");
            String image_name = this.getFileName(part);
            part.write(getServletContext().getRealPath("/")  + image_name);
            //response.sendRedirect("/cmart/NewItem.jsp");
        
            //ファイル名の変更
            String file_path = getServletContext().getRealPath("/") + image_name;
            String new_file_path = getServletContext().getRealPath("/") + name + ".jpg";
            File fOld = new File(file_path);
            File fNew = new File(new_file_path);
        
            if(fOld.exists()){
                fOld.renameTo(fNew);
            }else{
                System.out.println("ファイル名の変更に失敗しました");
            }
     		RequestDispatcher dispatch = request.getRequestDispatcher("Main.jsp");
            dispatch.forward(request, response); 
            rs2.close();
    	    rs.close();
            stmt.close();
            stmt2.close();
        }catch (ClassNotFoundException e){
            out.println("ClassNotFoundException:" + e.getMessage());
        }catch (SQLException e){
            out.println("SQLException:" + e.getMessage());
        }catch (Exception e){
          out.println("Exception:" + e.getMessage());
        }finally{
            try{
                if (conn != null){
                    conn.close();
                }
            }catch (SQLException e){
                out.println("SQLException:" + e.getMessage());
            }
        }
        out.close();
    }
    
    private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }
}

