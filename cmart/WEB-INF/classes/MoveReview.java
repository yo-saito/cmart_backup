import java.sql.*;
import java.io.*;
import java.net.*;
import java.lang.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import java.util.*;


@WebServlet("/MoveReview")
public class MoveReview extends HttpServlet {

    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String kagu = (String)request.getParameter("kagu");
        PrintWriter out = response.getWriter();
        request.setAttribute("kagu",kagu);
            
        RequestDispatcher dispatch = request.getRequestDispatcher("ReviewAdd.jsp");
        dispatch.forward(request, response);
        out.close();
    }
}

