<%@ page contentType="text/html; charset=utf-8" import="java.sql.*,java.lang.*" %>


<html>
<head>

<script>
window.onunload = function() {};
history.forward();
</script>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cmart!! | Ordering</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/AdminLTE-2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/AdminLTE-2.4.2/plagins/iCheck/square/blue.css">
  <link rel="stylesheet" href="AdminLTE-2.4.2/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="Test.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<!-- Site wrapper -->


<%
String user_name=(String)session.getAttribute("user_name");
if(user_name==null || user_name.isEmpty()){
  user_name="ログイン";
}
%>
<div class="wrapper">

  <header class="main-header">
    <!-- ロゴ -->
    <a href="Main.jsp" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>ma</span>
      <!-- logo for regular state and mobile devices -->
      <!--<span class="logo-lg"><b>C</b>mart!!</span>-->
      <img src="cmart-logo.png" height="55" width="210">
    </a>
    
      <nav class="navbar navbar-static-top" role="navigation">
        <form action = "Result" method = "GET"  onsubmit="return check();">
          <input name="kagu_name" type="text" id="SWord" class = "SearchWord"
          placeholder="物品名・カテゴリー" pattern="*.*"  maxlength='20' required>
          <button type="submit" class="Searchbtn">
            検索
          </button>
          <!--　ログイン状態変更処理　-->
        <!--　ログイン済状態　-->
        <%if(user_name!="ログイン"){%>
        <div class="navbar-custom-menu ">
          <!--<a href="Login.jsp" class="login-btn" style="font-size:28px">-->
          <!--ログイン-->
          <%=user_name%>さん
          <input type="button" value="ログアウト" onclick="location.href='Login.jsp'" class="btn-pos">
        </div>
        
        <!--　未ログイン状態　-->
        <%}else{%>
          <div class="navbar-custom-menu">
            <a href="Login.jsp" class="login-btn" style="font-size:28px">
              <!--ログイン-->
              <%=user_name%>
            </a>
          </div>
        <%}%>
        
        <!--　ログイン状態変更処理：ここまで　-->
        </form>
        
        
      </nav>
    </header>

  <!-- =============================================== -->
  <!-- 左側サイドバー関連 -->
  <aside class="main-sidebar">
    <!--<section class="sidebar">-->
      <!-- サイドバー内カテゴリー検索一覧 -->
      <!-- 下にコメントアウトの状態で<li>のテンプレートあり -->
    <!--  <ul class="sidebar-menu" data-widget="tree">-->
    <!--    <li class="header"><h2>カテゴリー</h2></li>-->
    <!--    <li class="treeview">-->
    <!--      <a href="SearchResult.jsp">-->
    <!--        <span>収納</span>-->
    <!--      </a>-->
    <!--    </li>-->
    <!--    <li class="treeview">-->
    <!--      <a href="SearchResult.jsp">-->
    <!--        <span>小物入れ</span>-->
    <!--      </a>-->
    <!--    </li>-->
    <!--    <li class="treeview">-->
    <!--      <a href="SearchResult.jsp">-->
    <!--        <span>その他</span>-->
    <!--      </a>-->
    <!--    </li>-->
    <!--    <li class="treeview">-->
    <!--      <a href="SearchResult.jsp">-->
    <!--        <span>add</span>-->
    <!--      </a>-->
    <!--    </li>-->
    <!--    <li class="treeview">-->
    <!--      <a href="SearchResult.jsp">-->
    <!--        <span>add</span>-->
    <!--      </a>-->
    <!--    </li>-->
    <!--  </ul>-->
    <!--</section>-->
    <!-- サイドバー関連：ここまで -->
  </aside>

<!-- =============================================== -->

  <!-- メインコンテンツ -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        発注確認
      </h1>
    </section>
    <!--発注物品情報受け取り-->
    <%
    	//HttpSession s = request.getSession();
    	request.setCharacterEncoding("UTF8");
    	String name = (String) request.getAttribute("name");
    	String cate = (String) request.getAttribute("cate");
    	String zai = (String) request.getAttribute("zai");
    	String setu = (String) request.getAttribute("setu");
    	String usad = (String) request.getAttribute("usad");
    %>
    <!--発注物品情報受け取り：ここまで-->
    <!-- メインコンテンツ -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <!--発注物品情報掲載-->
          <div class="info-box bg-yellow">
            <div class="info-box-contentS">
              <img src=<%=name + ".jpg"%> class="float-img" align="top">
              <table class="Item-infoM" border="2" rules="all">
                <tr>
                  <td class="setuU">物品名</td>
                  <td class="setuS"><%=name%></td>
                </tr>
                <tr>
                  <td>在庫数</td>
                  <td><%=zai%></td>
                </tr>
                <tr>
                  <td>説明</td>
                  <td><%=setu%></td>
                </tr>
              </table>
              <!-- /.info-box-content -->
              <input hidden type="text" class="user_name" value="<%=usad%>">
            </div>
          </div>
          <div class="col-md-3-2">
            <div class="box box-solid bg-yellow">
              <div class="input-group">
                <div class="input-group-btn btn-float4 ">
                  <%if(user_name!="ログイン"){%>
                  <form action="MailTest" method="POST">
                    <input type="hidden" name = kagu_name value="<%=name%>">
                    <input type="hidden" name = "user_name" value=<%=user_name%>> 
                    <input type="hidden" class="user_name" value="<%=usad%>">
                    <button id="disp3" type="submit" class="btn btn-primary btn-margin2" onclick="disp()">
                  <%}else{%>
                  <form action="Login.jsp">
                    <button id="disp3" type="submit" class="btn btn-primary btn-margin2-top" onclick="disp2()">
                    <%}%>
                      <a class="Searchbtn2">
                          確　　　定
                      </a>
                    </button>
                  </form>
                  <button id="disp2" type="submit" class="btn btn-primary btn-margin2-bottom">
                    <a href="Main.jsp" class="Searchbtn2">
                      メイン画面へ
                    </a>
                  </button>
                </div>
                <!-- /btn-group -->
              </div>
            </div>
          </div>
        <!--発注物品情報掲載：ここまで-->
        </div>
        <!--新規物品登録ページへのリンクブロック-->
        <section>
          <div class="row">
            <div class="col-md-3">
              <div class="box box-solid bg-red">
                <div class="box-body">
                  <p class="floatItem">
                    <%if(user_name!="ログイン"){%>
                    <a href="NewItem.jsp">
                  <%}else{%>
                    <a href="Login.jsp" onclick="disp2()">
                  <%}%>
                      <b>
                        新規物品登録
                      </b>
                      はこちら
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--新規物品登録ページへのリンクブロック：ここまで-->
        <!-- 下線 -->
          <div class="col-md-9">
            <div class="box box-warning">
              <div class="box-body no-padding">
                <!-- THE CALENDAR -->
                <div id="calendar">
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- 下線：ここまで -->
      </section>
    </div>
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
      </div>
      <strong>Copyright &copy; 2018 </strong> 
    </footer>
  <div class="control-sidebar-bg"></div>
</div>

<!--以下Script-->

<script>
function disp(){
	if(window.alert('発注が完了しました!!')){
		location.href = "Main.jsp";
	}
}
function disp2(){
	if(window.alert('ログインしてください!!')){
		location.href = "Main.jsp";
	}
}
var i=0;
        if(i<1){
            i++;
            document.getElementById("disp").style.display="none";
        }
        function a(num)
        {
          if (num == 0)
          {
            document.getElementById("disp").style.display="block";
            document.getElementById("disp2").style.display="none";
            document.getElementById("disp3").style.display="none";
            document.getElementById("disp4").style.display="none";
            
          }
          
        }
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
</body>
</html>
